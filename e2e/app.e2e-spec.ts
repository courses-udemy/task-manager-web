import { TaskManagerWebPage } from './app.po';

describe('task-manager-web App', () => {
  let page: TaskManagerWebPage;

  beforeEach(() => {
    page = new TaskManagerWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

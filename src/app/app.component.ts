import { Component } from '@angular/core';

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent
{
  title = 'Morroia!';

  task: Task = {
    id: 21,
    title: 'sadasdas'
  };

  task2: Task = new Task(22, 'sadasdas')
}

export class Task
{
  public id: number;
  public title: string;

  constructor(id: number, title: string) {
    this.id = id
    this.title = title
  }
}
